FROM node:8-alpine as builder

RUN apk add --no-cache curl tar

# 0.7.0
ARG COMMIT=c5fc5107cad2a476a03d7ce8427f1def41c20568

WORKDIR /src
RUN curl -Lso - https://github.com/zhukov/webogram/archive/${COMMIT}.tar.gz \
  | tar --strip-components 1 -xzvvf -

RUN yarn install
RUN yarn gulp build

FROM productionwentdown/caddy

COPY --from=builder /src/dist/ /srv/